import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from figure import Figure

params = {'figure.autolayout': True,
          'figure.dpi': 100,
          'axes.titlesize': 30, 'axes.labelsize': 40, 'font.size': 30, 'legend.fontsize': 30, 'xtick.labelsize': 30, 'ytick.labelsize': 30,
          'xtick.major.width': 1.2, 'xtick.minor.width':1.2,
          'ytick.major.width': 1.2, 'ytick.minor.width':1.2,
          'xtick.major.size': 8, 'xtick.minor.size':4,
          'ytick.major.size': 8, 'ytick.minor.size':4,          }

          #'figure.figsize': (8, 10),
plt.rcParams.update(params)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

data_file = "/Users/anna/Codes/Data/cosmosis_anna/lssxcmb/output/RSD/DESY1_data_halofit_takahashi_rsd_bestfit/growth_parameters/"
# nz_file = "/Users/anna/Google Drive/PhD/DES/SV/nz_tpzv1_old_bench-mark_bins/nz.txt"
nz_file = "/Users/anna/Google Drive/PhD/DES/Y1/Flask/dndz/dndz_5bins_wcmb_weighted2sig.dat"

A_d = 0.996
A_d_err = 0.027

A_d_sv= 0.73
A_d_sv_err = 0.16
nbin=5
# z_mean = [0.3, 0.5, 0.7, 0.9, 1.1]
z_mean = [0.24, 0.383, 0.526, 0.679, 0.826] #DES Y1 Redmagic Jack

bias = [1.521, 1.740, 1.670, 2.071, 2.123]
bias_err = [0.015, 0.012, 0.014, 0.017, 0.038]

def read_theor_D(data_file):
    z = np.loadtxt(data_file+"z.txt")
    Dz = np.loadtxt(data_file+"d_z.txt")
    Dz=Dz/Dz[0]
    return z, Dz

def growth_estimates(z, Dz, A_d, A_d_err):
    growth_fun=interp1d(z, Dz)
    Dz_estim = [growth_fun(np.array(z_mean[i])) for i in range(len(z_mean))]
    Dz_estim = [Dz_estim[i]*A_d for i in range(len(z_mean))]
    Dz_estim_err = [Dz_estim[i]*A_d_err for i in range(len(z_mean))]
    return Dz_estim, Dz_estim_err

if __name__ == '__main__':
    z, Dz = read_theor_D(data_file)
    Dz_estim, Dz_estim_err = growth_estimates(z, Dz, A_d, A_d_err)
    Dz_estim2, Dz_estim_err2 = growth_estimates(z, Dz, A_d_sv, A_d_sv_err)


    def poly_fun(z, a, b, c):
        return 1+a*z+b*z**2+c*z**3

    pol_coeff, _=curve_fit(poly_fun, z_mean, bias, sigma=bias_err)
    print pol_coeff
    print poly_fun(z_mean[0], pol_coeff[0], pol_coeff[1], pol_coeff[2])
    z_mean=np.array(z_mean)

    z=np.array(z)
    z_vec=np.arange(0.0, 1.0, 0.01)

    f, axarr = plt.subplots(2, 1, sharex=True, sharey=False, figsize=(16, 12), gridspec_kw=dict(hspace=0., left=0.13))
    axarr[1].set_ylabel("$D(z)$")
    axarr[1].set_xlabel("$z$")
    axarr[1].plot(z, Dz, color='black', linestyle='--', linewidth=1.5, label='$\Lambda$CDM prediction')
    axarr[1].errorbar(z_mean, Dz_estim, yerr=Dz_estim_err, marker='o', linestyle='', color=Figure.colors[0],markersize=10, linewidth=2.5)
    # axarr[1].errorbar(z_mean, Dz_estim2, yerr=Dz_estim_err2, marker='o', linestyle='', color=Figure.colors[1],markersize=10, linewidth=2.5, label='Giannantonio et al. 2016')
    axarr[1].set_xlim(right=1.0)
    axarr[1].set_ylim(bottom=0.5, top=1.09)
    axarr[1].legend(loc="upper right")
    axarr[0].set_ylabel("$b(z)$")
    axarr[0].errorbar(z_mean, bias, yerr=bias_err, marker='o', linestyle='', color=Figure.colors[0],markersize=10, linewidth=2.5)
    axarr[0].plot(z_vec,poly_fun(z_vec, pol_coeff[0], pol_coeff[1], pol_coeff[2]), color='black', linestyle='--', linewidth=2.5, label='Polynomial fit')
    axarr[0].legend(loc="lower right")
    # axarr[0].set_xlim(right=1.0)
    # axarr[0].set_ylim(bottom=0.4, top=1.2)
    plt.savefig("Dz_bias_estimates_DESY1.pdf")
    # plt.savefig("Dz_bias_estimates_DESY1.pdf")
    # plt.show()







# def find_zmean(nz_file):
#     nz=np.loadtxt(nz_file).T
#     print nz.shape
#     print nz[1]
#     z_mean = []
#     for i in range(1,nbin+1):
#         z_mean.append(np.mean(nz[i]))
#     return z_mean
