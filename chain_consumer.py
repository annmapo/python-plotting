#! /usr/bin/env python
import numpy as np
import pandas as pd
# from figure import Figure
import matplotlib.pyplot as plt
import pylab
from chainconsumer import ChainConsumer

class plotMCMC(object):
    """
    Reads a MCMC chain file and plots the posterior p.d.f.s.
    """
    # sample_labels = ["$b_1$", "$b_2$", "$b_3$", "$b_4$", "$b_5$", "$A_{D1}$", "$A_{D2}$", "$A_{D3}$", "$A_{D4}$", "$A_{D5}$"]
    # sample_labels = ["$b_1$", "$b_2$", "$b_3$", "$b_4$", "$A_{D1}$", "$A_{D2}$", "$A_{D3}$", "$A_{D4}$"]
    # truth_values = [1.03, 1.28, 1.32, 1.57, 1.95, 1.0, 1.0, 1.0, 1.0, 1.0]
    # sample_labels = ["$b_1$", "$b_2$", "$b_3$", "$b_4$", "$b_5$", "$A_{D}$"]
    # sample_labels = ["$b_1$", "$b_2$", "$b_3$", "$b_4$", "$A_{D}$"]
    # sample_labels = ["$b_1$", "$b_2$", "$b_3$", "$b_4$", "$b_5$", "$A_{L}$"]
    # sample_labels = ["$b_1$", "$b_2$", "$b_3$", "$b_4$", "$b_5$", "$A_{L1}$", "$A_{L2}$", "$A_{L3}$", "$A_{L4}$", "$A_{L5}$"]
    # truth_values = [1.45, 1.55, 1.65, 1.8, 2.0, 1.0]
    # truth_values = [1.89, 1.96, 1.93, 2.14, 1.036, 1.008, 1.078, 1.141, 1.0]
    # truth_values = [1.89, 1.96, 1.93, 2.14, 1.036, 1.008, 1.078, 1.141, 1.0, 1.0, 1.0, 1.0, 1.0]
    # truth_values = [1.89, 1.96, 1.93, 2.14, 1.0, 1.0, 1.0, 1.0]
    # truth_values = [1.74, 1.80, 1.83, 1.97, 1.0]
    # truth_values = [1.45, 1.55, 1.65, 1.8, 2.0, 1.0, 1.0]
    # truth_values = [1.45, 1.55, 1.65, 1.8, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    # truth_values = [1.45, 1.55, 1.65, 1.8, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    # sample_labels = ["$b_1^{S1}$", "$b_2^{S1}$", "$b_3^{S1}$", "$b_4^{S1}$", "$b_5^{S1}$","$b_6^{S1}$", "$b_1^{S2}$", "$b_2^{S2}$", "$b_3^{S2}$", "$b_4^{S2}$", "$b_5^{S2}$","$b_6^{S2}$","$A_{D}$", "$A_L$"]
    # sample_labels = ["$b_1^{S1}$", "$b_2^{S1}$", "$b_3^{S1}$", "$b_4^{S1}$", "$b_1^{S2}$", "$b_2^{S2}$", "$b_3^{S2}$", "$b_4^{S2}$","$A_{D}$"]
    # sample_labels = ["$b_1^{S1}$", "$b_2^{S1}$", "$b_3^{S1}$", "$b_4^{S1}$", "$b_1^{S2}$", "$b_2^{S2}$", "$b_3^{S2}$", "$b_4^{S2}$","$A_{D1}$", "$A_{D2}$", "$A_{D3}$", "$A_{D4}$", "$A_L$"]
    # truth_values = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 1.0, 1.0]
    sample_labels = ["$b_1$", "$b_2$", "$b_3$", "$b_4$", "$b_5$","$b_6$", "$A_D$"]
    # sample_labels = ["$\Omega_m$", "$h_0$", "$\Omega_b$", "$n_s$", "$A_s$", "$\omega$","$\\tau$", "$A_L$", "$\sigma_8$"]
    # sample_labels = ["$b_1$","$A_{D}$"]
    truth_values = [2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 1.0]
    walkers = 500
    # walkers = 10
    weights_bool = False

    burn_in = True
    # burned_tail = 182400
    # burned_tail = 373500
    # burned_tail = 400500
    # burned_tail = 100000
    # burned_tail = 442500
    # burned_tail = 396500
    # burned_tail = 402500
    burned_tail = 432500
    # burned_tail = 415500
    # burned_tail = 397500
    # burned_tail = 283500
    # burned_tail = 306500
    # burned_tail = 475000
    # burned_tail = 325000

    sample_cols = np.arange(len(sample_labels))
    chain_consumer = ChainConsumer()

    def __init__(self, path, name, ext):
        self.path = path
        self.name = name
        self.ext = ext
        self.read_data()
        self.set_sample()

    def read_data(self):
        if(self.burn_in):
            self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_labels, usecols=self.sample_cols, dtype=np.float64, comment="#").tail(self.burned_tail).values
            self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["like"],dtype=np.float64, comment="#").tail(self.burned_tail).values
        else:
            self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_labels, usecols=self.sample_cols, dtype=np.float64, comment="#").values
            if(self.weights_bool):
                self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)-1], names=["like"],dtype=np.float64, comment="#").values
                self.weights = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["weights"],dtype=np.float64, comment="#").values
            else:
                self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["like"],dtype=np.float64, comment="#").values
                self.weights = np.ones_like(self.like)
        print self.like.shape

    def set_sample(self):
        # self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like, walkers = self.walkers, name=self.name)
        if (self.weights_bool):
            self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like.T[0], weights=self.weights.T[0], walkers = self.walkers,name=self.name)
        else:
            self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like, walkers = self.walkers, name="Planck lensing")


    def config_chains(self):
        self.chain_consumer.configure(sigmas=[0, 1, 2], label_font_size=26, tick_font_size=12, legend_kwargs={"fontsize": 28}, shade=True,sigma2d=False, bins=0.9, statistics = "mean", summary=False)
        # self.chain_consumer.configure(sigmas=[0, 1, 2], summary=True)

    def triangle_plot(self):
        self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW", truth=self.truth_values)
        # self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW", legend=True)

    def convergence(self):
        # self.chain_consumer.plot_walks(truth=self.truth_values, filename=self.path+self.name+"_walks.pdf", plot_posterior=True )

        # Now, lets check our convergence using the Gelman-Rubin statistic
        gelman_rubin_converged = self.chain_consumer.diagnostic_gelman_rubin()
        # And also using the Geweke metric
        geweke_converged = self.chain_consumer.diagnostic.geweke()
        print "gelman_rubin_converged", gelman_rubin_converged
        print "geweke_converged", geweke_converged
        with open(self.path+"burned_tail.txt", "w") as text_file:
            text_file.write("burned tail = {}".format(self.burned_tail))
        # with open(self.path+"true_values.txt", "w") as text_file:
            # text_file.write("{}".format(self.truth_values))
        #
        # chain_divided=self.chain_consumer.divide_chain()
        # chain_divided.plot(filename = self.path+self.name+"_divided.pdf", figsize = "PAGE", truth=self.truth_values)

    def gen_latex(self):
        latex_table=self.chain_consumer.analysis.get_latex_table(transpose= True, hlines=True)
        with open(self.path+"latex_table.txt", "w") as text_file:
            text_file.write(latex_table)

    def combine_measurements(self, param_bottom, param_top):
        param_text = self.chain_consumer.get_summary()
        data_vec = [param_text[self.sample_labels[i]][1] for i in range(param_bottom,param_top)]
        cov = self.chain_consumer.get_covariance(parameters=self.sample_labels[param_bottom:param_top])[1]
        precision_matrix = np.linalg.inv(cov)

        cov_norm = np.empty_like(cov)
        for i in range(len(cov)):
            for j in range(len(cov)):
                cov_norm[i][j] = cov[i][j]/np.sqrt(cov[i][i]*cov[j][j])

        print cov_norm
        consensus_variance = 1./(np.sum(precision_matrix))

        self.consensus_data = consensus_variance*np.sum(np.sum(precision_matrix, axis=0)*data_vec)
        self.consensus_err = np.sqrt(consensus_variance)
        print "Consensus constraint is ", self.consensus_data, u"\u00B1", self.consensus_err

        with open(self.path+"consensus_constraint.txt", "w") as text_file:
            text_file.write("$A_D = %.2f \pm %.2f $"%(self.consensus_data, self.consensus_err))

        self.matrix_plot(cov_norm, "covariance_parameters_norm", param_top, param_bottom)


    def matrix_plot(self, matrix, name, param_top, param_bottom):
    	fig=plt.figure(figsize=(10,8))
    	ax=fig.add_subplot(111)
    	# cax=ax.imshow(matrix, cmap=pylab.cm.inferno)
    	cax=ax.imshow(matrix, cmap=pylab.cm.seismic, vmin=-1.0, vmax=1.0)
        plt.xticks(np.arange(param_top-param_bottom), self.sample_labels[param_bottom:param_top])
        plt.yticks(np.arange(param_top-param_bottom), self.sample_labels[param_bottom:param_top])
    	fig.colorbar(cax)
    	# plt.xlabel('bin $j$')
    	plt.ylabel('$C_{ij}/\sqrt{C_{ii}C_{jj}}$')
    	#changing the tick frequency:
    	# plt.locator_params(axis='x', nbins=20)
    	# plt.locator_params(axis='y', nbins=20)
    	# minorLocatorx=MultipleLocator(1)
    	# minorLocatory=MultipleLocator(1)
    	# ax.xaxis.set_minor_locator(minorLocatorx)
    	# ax.yaxis.set_minor_locator(minorLocatory)

    	plt.savefig(self.path+name+'.pdf')

if __name__ == '__main__':
    chain1 = plotMCMC("/Users/porredon/Codes/Data/cosmosis_anna/lssxcmb/output/RSD/emcee_Pop2_CMBlens_wDESSVnoise_nocorrection_old_ang2_v2_nl_v2/", "emcee_Pop2_CMBlens_wDESSVnoise_nocorrection_old_ang2_v2_nl_v2", ".txt")
    chain1.config_chains()
    chain1.convergence()
    chain1.triangle_plot()
    chain1.gen_latex()
    # chain1.combine_measurements(5, 10)
