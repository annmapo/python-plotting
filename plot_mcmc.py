#! /usr/bin/env python
import numpy as np
import pandas as pd
from getdist import plots, MCSamples, chains

class plotMCMC(object):
    """
    Reads a MCMC chain file and plots the posterior p.d.f.s.
    """
    sample_labels = ["b_1", "b_2", "b_3", "b_4", "b_5", "A^D_1", "A^D_2", "A^D_3", "A^D_4", "A^D_5"]
    sample_names = ["b1", "b2", "b3", "b4", "b5","d1", "d2", "d3", "d4", "d5"]
    burn = 60000 #number of samples to burn in
    sample_cols = np.arange(len(sample_names))
    ignore_rows = 0

    def __init__(self, path, name, ext):
        self.path = path
        self.name = name
        self.ext = ext
        self.read_data()
        self.set_sample()
        # self.convergence()

    def read_data(self):
        self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_names, usecols=self.sample_cols, dtype=np.float64, comment="#", skiprows=self.burn).values
        self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_names)], names=["like"],dtype=np.float64, comment="#", skiprows=self.burn).values
        self.weights = np.ones_like(self.like)
        print self.like.shape

    def set_sample(self):
        self.chains = []
        self.samples = MCSamples(ini="getdist_settings.ini", samples=self.samples_data, loglikes=self.like, names = self.sample_names, labels = self.sample_labels, files_are_chains=True)
        self.chains.append(self.samples)
        # print self.samples.shape
        # self.chains=np.empty_like(self.samples)
        # self.chains=self.samples
        self.samples.removeBurnFraction(self.ignore_rows)
        self.samples.deleteFixedParams()
        # self.samples.makeSingle()
        self.samples.updateBaseStatistics()
        print len(self.chains)

    def triangle_plot(self):
        self.tp = plots.getSubplotPlotter()
        self.tp.triangle_plot([self.samples], filled=True)
        self.tp.export(self.path+self.name+".pdf")

    def convergence(self):
        # self.samples.makeSingle()
        self.samples.getConvergeTests(self.samples.converge_test_limit, writeDataToFile=True, feedback=True)

if __name__ == '__main__':
    chain1 = plotMCMC("/Users/porredon/Codes/Data/cosmosis_anna/lssxcmb/output/RSD/emcee_galxcmb_photoz_cross_nokk_norsd_des_sv_wnoise_thetav3_v4/", "emcee_galxcmb_photoz_cross_nokk_norsd_des_sv_wnoise_thetav3_v4", ".txt")
    chain1.triangle_plot()
