#! /usr/bin/env python
import numpy as np
from scipy.interpolate import interp1d
import math

class Data(object):
    """
    Reads data from files and saves it into numpy arrays.

    The format of the data must be a single text file with the x-axis in the
    first column and the several y-axis in the other columns. Without headers.
    """

    def __init__(self, path):
        self.path = path
        self.read_data()

    def read_data(self):
        self.data = np.loadtxt(self.path).T

    def interpolate(self):
        self.data_fun = [interp1d(self.data[0], self.data[i]) for i in range(1, len(self.data))]

    def rad2deg(self):
        self.data[0] = [math.degrees(x) for x in self.data[0]]
