#! /usr/bin/env python
import numpy as np
from data import Data
from one_row_mult_cols import OneRowMultCol
from one_row_one_col import OneRowOneCol
from figure import Figure
from ratio import Ratio
from join_data import JoinData

path_dir = '/Users/porredon/Codes/Data/cosmosis_anna/lssxcmb/ana_mice_DM_norsd/'
path_dir2 = '/Users/porredon/Codes/cosmosis/halogen_4bins_linear_linbinning_nzv1.7_DM_test/galaxy_xi/'
path6 = '/Users/porredon/Codes/Data/cosmosis_anna/lssxcmb/ana_mice_DM_norsd/phi_4.dat'

path1 = path_dir+'linearv3/galaxy_xi/wtheta_linear_v3.txt'
path2 = path_dir+'nonlinearv3/galaxy_xi/wtheta_nonlinear_v3.txt'
path3 = path_dir+'linearv3/wl_number_density/z.txt'
path4 = path_dir+'linearv3/wl_number_density/bin_1.txt'
path5 = path_dir2+'wtheta2.txt'
# path6 = path_dir2+'pop1_bin1.txt'
# path7 = path_dir2+'pop1_bin2.txt'
# path8 = path_dir2+'pop1_bin3.txt'
# path9 = path_dir2+'pop1_bin4.txt'
# path10 = path_dir2+'pop1_bin5.txt'
# path11 = path_dir2+'pop1_bin6.txt'

data1 = Data(path1)
data2 = Data(path2)
data3 = Data(path3)
data4 = Data(path4)
data5 = Data(path5)
data6 = Data(path6)
# data7 = Data(path7)
# data8 = Data(path8)
# data9 = Data(path9)
# data10 = Data(path10)
# data11 = Data(path11)

# data_joined = JoinData(Data(path1).data, Data(path2).data)
# data5.data[1] = data5.data[1]/6

# print data1.data[0]
# data_joined.interpolate()
# z=data6.data[0]
# bin1_new = np.empty_like(z)
# bin2_new = np.empty_like(z)
# bin3_new = np.empty_like(z)
# bin4_new = np.empty_like(z)
# bin5_new = np.empty_like(z)
# for i in range (len(z)):
#     try:
#         bin1_new[i]= data_joined.data_fun[0](z[i])
#         bin2_new[i]= data_joined.data_fun[1](z[i])
#         bin3_new[i]= data_joined.data_fun[2](z[i])
#         bin4_new[i]= data_joined.data_fun[3](z[i])
#         bin5_new[i]= data_joined.data_fun[4](z[i])
#     except ValueError:
#         bin1_new[i] = 0.0
#         bin2_new[i] = 0.0
#         bin3_new[i] = 0.0
#         bin4_new[i] = 0.0
#         bin5_new[i] = 0.0

# print bin1_new
# print data_joined.data_fun[0](z)
# new_nz1= JoinData(z, bin1_new)
# new_nz2= JoinData(z, bin2_new)
# new_nz3= JoinData(z, bin3_new)
# new_nz4= JoinData(z, bin4_new)
# new_nz5= JoinData(z, bin5_new)
# total_nz= JoinData(z, bin1_new, bin2_new, bin3_new, bin4_new, bin5_new, Data(path6).data[1], Data(path7).data[1],
# Data(path8).data[1], Data(path9).data[1], Data(path10).data[1], Data(path11).data[1], bin1_new)
# total_nz2= JoinData(total_nz1, )
# new_nz2= JoinData(data1.data[0], data_joined.data_fun[1](z))
# new_nz3= JoinData(data1.data[0], data_joined.data_fun[2](z))
# new_nz4= JoinData(data1.data[0], data_joined.data_fun[3](z))
# new_nz5= JoinData(data1.data[0], data_joined.data_fun[4](z))
# new_nz6= JoinData(data1.data[0], data_joined.data_fun[5](z))
# print data6.interpolate().data_fun(data1.data[0])

data = JoinData(data3.data, data4.data)
# data2 = JoinData(data2.data, data3.data)
data1.rad2deg()
# data1.interpolate()
data2.rad2deg()
data5.rad2deg()
# data2.interpolate()

hline = [0.79508474970459231, 0.74330893465749182, 0.69158215907496379]
# hline =  [x**2.0 for x in hline]

# total_nz.save_data(path_dir2, "nz_DES_sv_x_pop1", ".txt")
# data = Ratio(data1, data2)

#Demo:
# plot = OneRowMultCol()
plot = OneRowOneCol()
plot.create_fig( "$\\theta$ [deg]", "$\omega(\\theta)$", "", "", data1.data, data2.data)
# plot.create_fig("$z$", "$\phi (z)$", "", "", data1.data, data2.data, data3.data, data4.data, data5.data)
# plot.create_fig("$z$", "$\phi (z)$", "", "", new_nz1.data, new_nz2.data, new_nz3.data, new_nz4.data, new_nz5.data)
# plot.create_fig("$z$", "$\phi (z)$", "", "", data.data)
# plot.h_lines(hline)
plot.save_figure(path_dir+"wtheta", "")
plot.show_fig()
