#! /usr/bin/env python
import numpy as np
import pandas as pd
from getdist import plots, MCSamples

class plotMCMC(object):
    """
    Reads a MCMC chain file and plots the posterior p.d.f.s.
    """
    sample_names = ["fsigma8" , "b1", "b2"]
    sample_labels = ["fsigma8" , "b1", "b2"]
    sample_cols = [4,5,6]
    num_file_cols = 19
    burn = 7000 #number of samples to burn in

    def __init__(self, path, name, ext):
        self.path = path
        self.name = name
        self.ext = ext
        self.read_data()
        self.set_sample()

    def read_data(self):
        self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_names, usecols=self.sample_cols, dtype=np.float64, comment="#", skiprows=self.burn).values
        chi2 = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[self.num_file_cols], names=["like"],dtype=np.float64, comment="#", skiprows=self.burn).values
        self.weights = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[0], names=["weight"],dtype=np.float64, comment="#", skiprows=self.burn).values
        self.like =  -0.5*chi2


    def set_sample(self):
        self.samples = MCSamples(ini="getdist_settings.ini", samples=self.samples_data, loglikes=self.like.T[0], weights=self.weights.T[0], names = self.sample_names, labels = self.sample_labels)

    def triangle_plot(self):
        self.tp = plots.getSubplotPlotter()
        self.tp.triangle_plot([self.samples], filled=True)
        self.tp.export(self.path+self.name+".pdf")

if __name__ == '__main__':
    chain1 = plotMCMC("/Users/anna/Google Drive/PhD/Euclid/", "chains_minerva_euclidcovs_300runs_3pkmulti_k0.17", ".dat")
    chain1.triangle_plot()
