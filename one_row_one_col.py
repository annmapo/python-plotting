#! /usr/bin/env python
from matplotlib import pyplot as plt
from figure import Figure

class OneRowOneCol(Figure):
    rows = 1
    cols = 1
    # kwargs = {xscale_log: False, yscale_log:False}

    def plot_data(self, data, count):
        self.axarr.set_ylabel(self.ylabel)
        # self.axarr.plot(data[0], data[1], color=Figure.colors[count], linestyle='-',marker='o', linewidth=1.5)
        self.axarr.plot(data[0], data[1], color=Figure.colors[count], linestyle='-', linewidth=1.5)
        self.axarr.set_xlabel(self.xlabel)
        # self.axarr.axhline(y=1.0, color='k', linestyle = '--', linewidth=1.5)
        # self.axarr.set_xlim(right=5.0)
        # self.axarr.set_ylim(bottom = 0.7, top =1.2)

    def set_xlog(self):
        self.axarr.set_xscale('log')

    def set_ylog(self):
        self.axarr.set_yscale('log')

    def h_lines(self, data):
        self.axarr.axhline(y=data[1], color='k', linestyle = '--', linewidth=1.5)

    def create_fig(self, xlabel, ylabel, xlim, ylim,  *args):
    # def create_fig(self, cols, xlabel, ylabel, *args, **kwargs):
        """
        Plots a figure with 1 row and 1 column.

        The arguments are: xlabel, ylabel, data1, [data2, ...],
        xscale_log = True or False and yscale_log = True or False.
        """
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.xlim = xlim
        self.ylim = ylim
        self.f, self.axarr = plt.subplots(self.rows, self.cols, sharex=True, sharey=True)
        self.f.subplots_adjust(hspace=0)
        for count, data in enumerate(args):
            self.plot_data(data, count)

        self.set_xlog()
        self.set_ylog()
        # if(self.kwargs.key["xscale_log"]):
        # if(self.kwargs.key["yscale_log"]):
