#! /usr/bin/env python
import numpy as np
import pandas as pd
from astropy.table import Table

class Catalog(object):
    """
    Reads table from file and saves it to a pandas dataframe.
    """
    z_edges = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2]

    def __init__(self, path, name, ext):
        self.path = path
        self.name = name
        self.ext = ext
        self.read_data()

    def read_data(self):
        # self.df = Table.read(self.path, format='ascii').to_pandas()
        self.df = pd.read_table(self.path+self.name+self.ext, header=0, sep=" ")

    def remove_duplicates(self):
        self.df = self.df[~self.df.duplicated('coadd_objects_id')]

    def photoz_cuts(self):
        for i in range(len(self.z_edges)-1):
            self.bin_sel = self.df.loc[(self.df.tpz_v1 >= self.z_edges[i]) & (self.df.tpz_v1 <= self.z_edges[i+1])]
            self.num_gal = len(self.bin_sel['coadd_objects_id'])
            print 'number of galaxies per bin:', self.num_gal
            self.hist, self.bin_edges = np.histogram(self.bin_sel['tpz_v1'], bins=100, normed=True)
            self.save_data(i)

    def save_data(self, ibin):
        np.savetxt(self.path+self.name+"_nz_bin"+str(ibin+1)+".txt", zip(self.bin_edges[0:-1], self.hist), delimiter='      ')

if __name__ == '__main__':
    data = Catalog("/Users/porredon/Data/DES SV/", "benchmark_old_photometry_TPZv1_BPZv2_wavg_tpc_badflag_type", ".ssv")
    data.remove_duplicates()
    data.photoz_cuts()

    print data.df.head()
    print data.df.dtypes
    # print data.df.coadd_objects_id
    print data.df[data.df.duplicated('coadd_objects_id')].shape
