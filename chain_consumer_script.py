#! /usr/bin/env python
import numpy as np
import pandas as pd
from figure import Figure
import matplotlib.pyplot as plt
import pylab
from chainconsumer import ChainConsumer

#Name and path of the chain is given at the end of the file, when you call the plotMCMC class.

class plotMCMC(object):
    """
    Reads a MCMC chain file and plots the posterior p.d.f.s.
    """

    #Names of all the parameters in the file. If you write in LaTeX the names will be rendered in the plots
    sample_labels = ["$\Omega_m$", "$h_0$", "$\Omega_b$", "$n_s$", "$A_s$", "$\omega$","$\\tau$", "$A_L$", "$\sigma_8$"]

    #Fiducial values of the parameters. Only needed if you want to plot them as lines
    truth_values = [0.295, 0.6881, 0.0468, 0.9676, 2.260574e-09, 0.0006155, -1.0, 0.08, 1.0, 0.83]

    weights_bool = True #If the chain uses weights

    burn_in = False

    burned_tail = 400500 #Not used if burn_in is False


    sample_cols = np.arange(len(sample_labels))
    chain_consumer = ChainConsumer()

    def __init__(self, path, name, ext):
        self.path = path
        self.name = name
        self.ext = ext
        self.read_data()
        self.set_sample()

    def read_data(self):
        if(self.burn_in):
            self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_labels, usecols=self.sample_cols, dtype=np.float64, comment="#").tail(self.burned_tail).values
            self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["like"],dtype=np.float64, comment="#").tail(self.burned_tail).values
        else:
            if(self.weights_bool):
                self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_labels, usecols=self.sample_cols, dtype=np.float64, comment="#").values
                self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["like"],dtype=np.float64, comment="#").values
                self.weights = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)+1], names=["weights"],dtype=np.float64, comment="#").values
            else:
                self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_labels, usecols=self.sample_cols, dtype=np.float64, comment="#").values
                self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["like"],dtype=np.float64, comment="#").values
                self.weights = np.ones_like(self.like)
        print self.like
        print self.weights

    def set_sample(self):
        if (self.weights_bool):
            self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like.T[0], weights=self.weights.T[0],name=self.name)
        else:
            self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like, walkers = self.walkers, name=self.name)


    def config_chains(self):
        self.chain_consumer.configure(sigmas=[0, 1, 2], label_font_size=26, tick_font_size=18, legend_kwargs={"fontsize": 28}, shade=True,sigma2d=False, bins=0.9, statistics = "mean")

    def triangle_plot(self):
        #Examples of different plots, if not specified it will plot all the parameters of the file.
        # self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW", truth=self.truth_values)
        # self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW", parameters=["$h_0$", "$\omega$"])
        self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW")

    def convergence(self):

        # Now, lets check our convergence using the Gelman-Rubin statistic
        gelman_rubin_converged = self.chain_consumer.diagnostic_gelman_rubin()
        # And also using the Geweke metric
        geweke_converged = self.chain_consumer.diagnostic.geweke()
        print "gelman_rubin_converged", gelman_rubin_converged
        print "geweke_converged", geweke_converged
        with open(self.path+"burned_tail.txt", "w") as text_file:
            text_file.write("burned tail = {}".format(self.burned_tail))
        with open(self.path+"true_values.txt", "w") as text_file:
            text_file.write("{}".format(self.truth_values))

    def gen_latex(self):
        '''Writes a file with the mean values and sigma in a latex table'''
        latex_table=self.chain_consumer.analysis.get_latex_table(transpose= True, hlines=True)
        with open(self.path+"latex_table.txt", "w") as text_file:
            text_file.write(latex_table)

if __name__ == '__main__':
    #You need to give the path, name and extension of the chain file to plotMCMC()
    chain1 = plotMCMC("/Users/porredon/Codes/Data/cosmosis_anna/lssxcmb/output/RSD/planck_likelihood_2_2_final_emcee/", "planck_likelihood_2_2_final_emcee", ".txt")
    chain1.config_chains()
    # chain1.convergence() #This is to get some statistics about convergence
    chain1.triangle_plot()
    chain1.gen_latex()
