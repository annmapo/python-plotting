#! /usr/bin/env python
from data import Data
import numpy as np

class Ratio(object):
    """
    Returns the ratio between two data objects.
    """

    def __init__(self, data1, data2):
        self.data1 = data1
        self.data2 = data2
        self.data1.interpolate()
        self.data2.interpolate()

        self.ratio()

    def check_range(self):
        min1 = np.amin(self.data1.data[0])
        min2 = np.amin(self.data2.data[0])
        max1 = np.amax(self.data1.data[0])
        max2 = np.amax(self.data2.data[0])

        self.min = max(min1, min2)
        self.max = min(max1, max2)


    def ratio(self):
        self.check_range()
        self.data = np.empty_like(self.data1.data)
        self.data[0] = np.linspace(self.min, self.max, num=40)
        # print self.data.shape, len(self.data1.data_fun)
        for i in range(1, len(self.data1.data_fun)+1):

            self.data[i] = self.data1.data_fun[i-1](self.data[0])/self.data2.data_fun[i-1](self.data[0])
            print self.data[0], self.data[i]
