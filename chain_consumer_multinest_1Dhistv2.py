#! /usr/bin/env python
import numpy as np
import pandas as pd
from figure import Figure
import matplotlib.pyplot as plt
import pylab
from chainconsumer import ChainConsumer

colors=["#ff982a", "#00a0d2", "#42b933","#9f211c","#792e9f","#f3bf28","#02d5cf", "#ff71d6", "#3f5086"," #286d00","#9383ff" ]
class plotMCMC(object):
    """
    Reads a MCMC chain file and plots the posterior p.d.f.s.
    """
    # sample_labels = ["$\Omega_m$", "$h_0$", "$\Omega_b$", "$n_s$", "$A_s$", "$\Omega_{\\nu}h^2$", "$\omega$","$\\tau$", "$A_L$", "$r_s(z_{drag})$"]
    # sample_labels = ["$\Omega_m$", "$h_0$", "$\Omega_b$", "$n_s$", "$A_s$", "$\omega$","$\\tau$", "$A_L$", "$\sigma_8$"]
    # sample_labels = ["$\Omega_m$", "$h_0$", "$\Omega_b$", "$n_s$", "$A_s$","$\Omega_{\\nu}h^2$",  "$\omega$","$b^1$", "$b^2$", "$b^3$", "$b^4$", "$b^5$",
    # "$m^1$", "$m^2$", "$m^3$", "$m^4$", "$A_{IA}$", "\eta_{IA}", "$\Delta z_l^1$", "$\Delta z_l^2$", "$\Delta z_l^3$", "$\Delta z_l^4$", "$\Delta z_l^5$",
    # "$\Delta z_s^1$", "$\Delta z_s^2$", "$\Delta z_s^3$", "$\Delta z_s^4$", "$\Delta z_s^5$", "$r_d$", "$D_A(z_{eff})$" "$\sigma_8$"]
    sample_labels = ["$r_d$", "$D_A(z_{eff})$"]
    truth_values = [0.295, 0.6881, 0.0468, 0.9676, 2.260574e-09, 0.0006155, -1.0, 0.08, 1.0, 0.83]
    walkers = 500
    # walkers = 10
    weights_bool = True

    burn_in = False

    burned_tail = 400500
    nsample=13627
    D_a_rd_fid = 10.41


    sample_cols = np.arange(len(sample_labels))
    print sample_cols, len(sample_labels)
    chain_consumer = ChainConsumer()

    def __init__(self, path, name, ext, path2, name2, ext2):
        self.path1 = path
        self.path2 = path2
        self.name1 = name
        self.name2 = name2
        self.ext1 = ext
        self.ext2 = ext2
        self.read_data()
        self.set_sample()

    def read_data(self):
        fullname1 = self.path1+self.name1+self.ext1
        fullname2 = self.path2+self.name2+self.ext2
        if(self.burn_in):
            self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_labels, usecols=self.sample_cols, dtype=np.float64, comment="#").tail(self.burned_tail).values
            self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["like"],dtype=np.float64, comment="#").tail(self.burned_tail).values
        else:
            if(self.weights_bool):
                self.samples_data = pd.read_table(fullname1, header=None, delim_whitespace=True, names = self.sample_labels, usecols=[26, 27], dtype=np.float64, comment="#").values
                self.like = pd.read_table(fullname1, header=None, delim_whitespace=True, usecols=[30], names=["like"],dtype=np.float64, comment="#").values
                self.samples_data_2 = pd.read_table(fullname2, header=None, delim_whitespace=True, names = ["alpha"], usecols=[0], dtype=np.float64, comment="#").values
                self.chi2_2 = pd.read_table(fullname2, header=None, delim_whitespace=True, usecols=[1], names=["like"],dtype=np.float64, comment="#").values
                self.weights = pd.read_table(fullname1, header=None, delim_whitespace=True, usecols=[29], names=["weights"],dtype=np.float64, comment="#").values
            else:
                self.samples_data = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, names = self.sample_labels, usecols=self.sample_cols, dtype=np.float64, comment="#").values
                self.like = pd.read_table(self.path+self.name+self.ext, header=None, delim_whitespace=True, usecols=[len(self.sample_labels)], names=["like"],dtype=np.float64, comment="#").values
                self.weights = np.ones_like(self.like)
        print self.like
        print self.weights
        self.new_sample = np.zeros_like(self.samples_data.T[0])
        self.new_sample_2 = np.zeros_like(self.samples_data_2)
        self.like_2 = np.zeros_like(self.samples_data_2)
        self.new_sample =  self.samples_data.T[1]/self.samples_data.T[0]
        self.new_sample_2 =self.samples_data_2 * self.D_a_rd_fid
        self.like_2 = -0.5*self.chi2_2
        print self.samples_data
        # print self.new_sample_2, self.like_2

    def set_sample(self):
        # self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like, walkers = self.walkers, name=self.name)
        if (self.weights_bool):
            # self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like.T[0], weights=self.weights.T[0], walkers = self.walkers,name=self.name)
            self.chain_consumer.add_chain(self.samples_data, parameters=["$r_d$", "$D_A(z_{\mathrm{eff}})$"], posterior = self.like.T[0], weights=self.weights.T[0],name="DES 3x2pt")
            # self.chain_consumer.add_chain(self.new_sample_2, parameters=["$D_A(z_{\mathrm{eff}})/r_d$"], posterior = self.like_2, name="DES Y1 BAO")
        else:
            self.chain_consumer.add_chain(self.samples_data, parameters=self.sample_labels, posterior = self.like, walkers = self.walkers, name=self.name)


    def config_chains(self):
        # self.chain_consumer.configure(sigmas=[0, 1, 2], label_font_size=26, tick_font_size=18, legend_kwargs={"fontsize": 28}, shade=True,sigma2d=False, bins=0.9, statistics = "mean")
        self.chain_consumer.configure(sigmas=[0, 1, 2], label_font_size=26, tick_font_size=18, legend_kwargs={"fontsize": 14}, sigma2d=False,
        colors=[colors[0], colors[1]], shade=[True, True], linewidths=[1.0, 2.0], linestyles=["--", "-"], shade_alpha =[0.5, 0.5], bins=0.9,
        statistics = "mean", kde=2.0, summary=False, legend_artists = False)
        # self.chain_consumer.configure(sigmas=[0, 1, 2], summary=True)

    def triangle_plot(self):
        # self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW", truth=self.truth_values)
        # self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW", parameters=["$\Omega_m$", "$h_0$", "$\Omega_b$", "$n_s$", "$A_s$", "$\omega$","$\\tau$", "$A_L$", "$\sigma_8$"])
        # self.chain_consumer.plotter.plot(filename = self.path+self.name+".pdf", figsize = "GROW", parameters=["$h_0$", "$\omega$"])
        self.chain_consumer.plotter.plot(filename = self.path1+"1d_comp_rd.pdf", figsize = "COLUMN", legend=True, parameters = ["$r_d$"])

    def convergence(self):
        # self.chain_consumer.plot_walks(truth=self.truth_values, filename=self.path+self.name+"_walks.pdf", plot_posterior=True )

        # Now, lets check our convergence using the Gelman-Rubin statistic
        gelman_rubin_converged = self.chain_consumer.diagnostic_gelman_rubin()
        # And also using the Geweke metric
        geweke_converged = self.chain_consumer.diagnostic.geweke()
        print "gelman_rubin_converged", gelman_rubin_converged
        print "geweke_converged", geweke_converged
        with open(self.path+"burned_tail.txt", "w") as text_file:
            text_file.write("burned tail = {}".format(self.burned_tail))
        with open(self.path+"true_values.txt", "w") as text_file:
            text_file.write("{}".format(self.truth_values))
        #
        # chain_divided=self.chain_consumer.divide_chain()
        # chain_divided.plot(filename = self.path+self.name+"_divided.pdf", figsize = "PAGE", truth=self.truth_values)

    def gen_latex(self):
        latex_table=self.chain_consumer.analysis.get_latex_table(transpose= True, hlines=True)
        with open(self.path+"latex_table.txt", "w") as text_file:
            text_file.write(latex_table)

    def combine_measurements(self, param_bottom, param_top):
        param_text = self.chain_consumer.get_summary()
        data_vec = [param_text[self.sample_labels[i]][1] for i in range(param_bottom,param_top)]
        cov = self.chain_consumer.get_covariance(parameters=self.sample_labels[param_bottom:param_top])[1]
        precision_matrix = np.linalg.inv(cov)

        cov_norm = np.empty_like(cov)
        for i in range(len(cov)):
            for j in range(len(cov)):
                cov_norm[i][j] = cov[i][j]/np.sqrt(cov[i][i]*cov[j][j])

        print cov_norm
        consensus_variance = 1./(np.sum(precision_matrix))

        self.consensus_data = consensus_variance*np.sum(np.sum(precision_matrix, axis=0)*data_vec)
        self.consensus_err = np.sqrt(consensus_variance)
        print "Consensus constraint is ", self.consensus_data, u"\u00B1", self.consensus_err

        with open(self.path+"consensus_constraint.txt", "w") as text_file:
            text_file.write("$A_D = %.2f \pm %.2f $"%(self.consensus_data, self.consensus_err))

        self.matrix_plot(cov_norm, "covariance_parameters_norm", param_top, param_bottom)


    def matrix_plot(self, matrix, name, param_top, param_bottom):
    	fig=plt.figure(figsize=(10,8))
    	ax=fig.add_subplot(111)
    	# cax=ax.imshow(matrix, cmap=pylab.cm.inferno)
    	cax=ax.imshow(matrix, cmap=pylab.cm.seismic, vmin=-1.0, vmax=1.0)
        plt.xticks(np.arange(param_top-param_bottom), self.sample_labels[param_bottom:param_top])
        plt.yticks(np.arange(param_top-param_bottom), self.sample_labels[param_bottom:param_top])
    	fig.colorbar(cax)
    	# plt.xlabel('bin $j$')
    	plt.ylabel('$C_{ij}/\sqrt{C_{ii}C_{jj}}$')
    	#changing the tick frequency:
    	# plt.locator_params(axis='x', nbins=20)
    	# plt.locator_params(axis='y', nbins=20)
    	# minorLocatorx=MultipleLocator(1)
    	# minorLocatory=MultipleLocator(1)
    	# ax.xaxis.set_minor_locator(minorLocatorx)
    	# ax.yaxis.set_minor_locator(minorLocatory)

    	plt.savefig(self.path+name+'.pdf')

if __name__ == '__main__':
    chain1 = plotMCMC("/Users/porredon/Codes/Data/cosmosis_anna/lssxcmb/output/RSD/2pt_NG.fits_d_l_chain/", "fits_d_l_chain_samples_short_v3", ".txt", "/Users/porredon/Codes/Data/cosmosis_anna/lssxcmb/output/RSD/2pt_NG.fits_d_l_chain/", "alpha_likelihood_cosmo1_NAP4_Cov2_zbin4_1234_dtheta0.30_DNF-1", ".txt")
    chain1.config_chains()
    # chain1.convergence()
    chain1.triangle_plot()
    # chain1.gen_latex()
    # chain1.combine_measurements(5, 10)
