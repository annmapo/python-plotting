#! /usr/bin/env python

import numpy as np
import os
from data import Data

path_dir1 = os.environ["HOME"]+"/Codes/DES-cov-code/ACF/"
path_dir2 = os.environ["HOME"]+"/Google Drive/PhD/DES/Y1/Flask/Data/"
path_dir3 = os.environ["HOME"]+"/Codes/Data/cosmosis_anna/lssxcmb/output/RSD/"
fname_1 = "wtheta_linear_CMBlens_wDESSVnoise_Y1test_gaussbeam"
fname_2 = "lg30.dat"
dataf_1 = path_dir1+fname_1+".dat"
dataf_2 = path_dir1+fname_1+"_lg30"+".dat"
nbin = 6

class lFiltering(object):

    def __init__(self):
        wtheta_orig = Data(dataf_1)
        wtheta_l_filter = Data(dataf_2)
        theta = wtheta_orig.data[0]
        wtheta_kg = np.empty([nbin -1 , len(theta)])
        wtheta_kg_l_filter = np.empty([nbin -1 , len(theta)])
        l_filter = np.empty([nbin -1 , len(theta)])
        index = nbin
        for i in range(1,nbin):
            wtheta_kg[i-1] = wtheta_orig.data[index]
            wtheta_kg_l_filter[i-1] = wtheta_l_filter.data[index]
            index = index + nbin - (i)

        l_filter = wtheta_kg - wtheta_kg_l_filter
        header = "theta [deg]   g-kappa bin1      bin2        ..."
        print "Saving l filter correction to ", path_dir2+fname_2
        np.savetxt(path_dir2+fname_2, np.vstack([theta, l_filter]).T, header=header, delimiter="      ")

    # def apply(self):




if __name__ == '__main__':
    l_filter = lFiltering()
