#! /usr/bin/env python
from matplotlib import pyplot as plt

class Figure(object):
    """
    Figure object.

    Available colors are, respectively: orange, light blue, light green, red,
    purple, yellow, turquoise, pink, dark blue, dark green, violet
    """

    colors=["#ff982a", "#00a0d2", "#42b933","#9f211c","#792e9f","#f3bf28","#02d5cf", "#ff71d6", "#3f5086"," #286d00","#9383ff" ]

    def __init__(self):
        self.set_plot_params()

    def set_plot_params(self):
        params = {'figure.autolayout': False,
          'figure.dpi': 100,
          'axes.titlesize': 25, 'axes.labelsize': 22, 'font.size': 18, 'legend.fontsize': 12, 'xtick.labelsize': 14, 'ytick.labelsize': 14,
          'xtick.major.width': 1.2, 'xtick.minor.width':1.2,
          'ytick.major.width': 1.2, 'ytick.minor.width':1.2,
          'xtick.major.size': 8, 'xtick.minor.size':4,
          'ytick.major.size': 8, 'ytick.minor.size':4,          }

        plt.rcParams.update(params)

    def set_xlog(self):
        for i in range(0,self.cols):
            for j in range(0,self.rows):
                self.axarr[j, i].set_xscale('log')

    def set_ylog(self):
        for i in range(0,self.cols):
            for j in range(0,self.rows):
                self.axarr[j, i].set_yscale('log')

    def save_figure(self, name, output_dir):
        self.name = name
        self.output_dir = output_dir
        plt.savefig(self.output_dir+self.name+".pdf")

    def show_fig(self):
        plt.show()
