#! /usr/bin/env python
import numpy as np
from scipy.interpolate import interp1d
from data import Data
import math

class JoinData(object):
    """
    Joins columns from several data files.
    """

    def __init__(self, *args):
        self.format(args)
        self.n_points = len(self.x_arr)
        self.data = np.empty([self.n_arr, self.n_points])
        if args[0].ndim == 1:
            self.data = np.vstack(args)
        else:
            for count, arg in enumerate(args):
                if count==0:
                    self.data = [arg[i] for i in range(len(arg))]
                else:
                    if np.array_equiv(arg[0], self.data[0]):
                        for i in range(len(arg)-1):
                            self.data= np.vstack([self.data, arg[i+1]])
                    else:
                        for i in range(len(arg)):
                            self.data= np.vstack([self.data, arg[i]])

    def format(self, *args):
        self.n_arr = 0
        for count, arg in enumerate(args):
            if count == 0:
                self.x_arr = arg[0]
                self.n_arr = self.n_arr + len(arg)
            else:
                if np.array_equal(arg[0], self.x_arr):
                    self.n_arr = self.n_arr + len(arg - 1)
                else:
                    self.n_arr = self.n_arr + len(arg)

    def save_data(self, path, name, ext):
        print np.array(self.data).shape
        np.savetxt(path+name+ext, np.array(self.data).T)

    def rad2deg(self):
        self.data[0] = [math.degrees(x) for x in self.data[0]]

    def interpolate(self):
        self.data_fun = [interp1d(self.data[0], self.data[i]) for i in range(1, len(self.data))]
